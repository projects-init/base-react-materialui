# Cài đặt package

```bash
npm install --save
```
Install for dev
```bash
npm install --savedev
```
# Cấu trúc thư mục
### assets
- Chứa các tài nguyên của dự án: ảnh, font chữ, css, translate,...
### components
- Tập hợp các component dùng chung : Button, Tab, Table, Checkbox, ... sử dụng trên các file trong dự án.
### constants
- Định nghĩa các hằng số dùng trong dự án
### content
- Nội dung của từng page
### layouts
- Tập hợp các layout được dùng trong Prj: Login, Status Page, Main Page,...
### models
- Định nghĩa trường thông tin của đối tượng
### services
- Tập hợp các request đến api
### store (Nếu sử dụng redux)
- Quản lý các state của ứng dụng
### theme
### untils
- Tập hợp các chức năng tiện ích được sử dụng nhiều lần trong dự án

# Todo
 
# Lưu ý khi code
- Comment đầy đủ các function bao gồm các thông tin: Tên function, mô tả, khai báo biến (kiểu dữ liệu), kết quả trả về (kiểu dữ liệu)
- Comment giải thích các logic nghiệp vụ rắc rối
- Tránh để màn hình console xuất hiện warning và error 

# Lưu ý khi merge code
- Khi commit và merge code từ nhánh chức năng vào nhánh chính cần chú ý để tránh conflict
- Để tránh ít conflict và xử lý conflict dễ dàng thì có thể thực hiện rebase như sau:
```
```
B1: Checkout sang nhánh chính
```bash
git checkout develop
```

B2: Pull code từ nhánh chính trên git về local

```bash
git pull origin develop
```

B3: Checkout sang nhánh chức năng đang code
```bash
git checkout my-feature
```

B4: Thực hiện rebase từ nhánh chính
```bash
git rebase develop
```

B5: Nếu xảy ra conflict thì thực hiện xử lý conflict rồi commit lại code
```bash
git add "file xử lý conflic"
git commit -m "handle conflicts ..."
```

B6: Thực hiện tiếp quá trình rebase
```bash
git rebase --continue
```
B7: Quay lại bước 4 nếu tiếp tục có conflict
```
```
B8: Push code từ local lên git
```bash
git push origin my-feature
```
B9: Thực hiện tạo merge request từ nhánh chức năng vào nhanh chính
```
```
B10: Kiểm tra lại các file thay đổi của từng commit
# License
[MIT](https://choosealicense.com/licenses/mit/)