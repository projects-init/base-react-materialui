import { ReactNode } from 'react';

import BackupTableIcon from '@mui/icons-material/BackupTable';
import { ROLE_MENU } from 'src/constants/roleMenu';

export interface MenuItem {
  id: string
  parent: string;
  name: string;
  role: any[];
  items?: MenuItem[];
  link?: string;
  icon?: ReactNode;
  badge?: string;
}

export interface MenuItems {
  id: string,
  heading: string;
  role: any[]
  items: MenuItem[];
}

const menuItems: MenuItems[] = [
  {
    id: '0',
    heading: '',
    role: ["admin"],
    items: [
      {
        id: '0_1',
        parent: '0',
        name: 'news',
        link: '/feed',
        icon: BackupTableIcon,
        role: ["admin"]
      }
    ],
  }
];

export default menuItems;
