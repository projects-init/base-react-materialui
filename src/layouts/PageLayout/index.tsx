import { FC, ReactNode } from "react";
import PageTitleWrapper from "src/components/PageTitleWrapper";
import PageTitle from "src/components/PageTitle";
import { Box, Container, styled } from "@mui/material";

interface PageLayoutProps {
  children?: ReactNode;
  title?: string;
  subTitle?: string;
}

const MainContent = styled(Box)(
  ({ theme }) => `
    border-radius: 1px;
    padding: 10px;
    background: ${theme.colors.alpha.white[100]};
  `
);

const PageLayout: FC<PageLayoutProps> = ({ children, title, subTitle }) => {
  return (
    <>
      <PageTitleWrapper>
        <PageTitle heading={title} subHeading={subTitle} />
      </PageTitleWrapper>

      {/* Content page */}
      <Container maxWidth="xl">
        <MainContent>
          {children}
        </MainContent>
      </Container>
    </>
  );
};

export default PageLayout;
