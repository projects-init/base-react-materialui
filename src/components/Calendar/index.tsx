import { createDaysForCurrentMonth, createDaysForNextMonth, createDaysForPreviousMonth } from "./helper";

const Calendar = ({
    className = "",
    yearAndMonth = [2021, 6],
    onYearAndMonthChange,
    renderDay = () => null
  }) => {
    const [year, month] = yearAndMonth;
  
    let currentMonthDays = createDaysForCurrentMonth(year, month);
    let previousMonthDays = createDaysForPreviousMonth(
      year,
      month,
      currentMonthDays
    );
    let nextMonthDays = createDaysForNextMonth(year, month, currentMonthDays);
    let calendarGridDayObjects = [
      ...previousMonthDays,
      ...currentMonthDays,
      ...nextMonthDays
    ];
  
    const handleMonthNavBackButtonClick = () => {
      let nextYear = year;
      let nextMonth = month - 1;
      if (nextMonth === 0) {
        nextMonth = 12;
        nextYear = year - 1;
      }
      onYearAndMonthChange([nextYear, nextMonth]);
    };
  
    const handleMonthNavForwardButtonClick = () => {
      let nextYear = year;
      let nextMonth = month + 1;
      if (nextMonth === 13) {
        nextMonth = 1;
        nextYear = year + 1;
      }
      onYearAndMonthChange([nextYear, nextMonth]);
    };
  
    const handleMonthSelect = (evt) => {
      let nextYear = year;
      let nextMonth = parseInt(evt.target.value, 10);
      onYearAndMonthChange([nextYear, nextMonth]);
    };
  
    const handleYearSelect = (evt) => {
      let nextMonth = month;
      let nextYear = parseInt(evt.target.value, 10);
      onYearAndMonthChange([nextYear, nextMonth]);
    };
}
// https://codesandbox.io/s/full-page-calendar-part-one-veh84?from-embed=&file=/src/calendar.js:609-618

export default Calendar;