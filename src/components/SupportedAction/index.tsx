import Button from '@mui/material/Button';
import { withTranslation } from 'react-i18next';
import { IconButton, Tooltip } from '@mui/material';
import { Add, EditAttributes, RestartAlt, PowerSettingsNew, SettingsBackupRestore } from '@mui/icons-material';
interface confirmProps {
  name?: string;
  key?: any;
  color?: 'primary' | 'secondary' | 'error' | 'warning' | 'success' | 'info';
  onClick?: any;
  iconName?: string;
  isDisable?: boolean;
  t?: any;
}

const listIcon = {
  PowerSettingsNew: <PowerSettingsNew fontSize='small' />,
  SettingsBackupRestore: <SettingsBackupRestore fontSize='small'/>,
  RestartAlt: <RestartAlt fontSize='small'/>,
  EditAttributes: <EditAttributes fontSize='small'/>,
  Add: <Add fontSize='small'/>
}

function ActionBtn(props: confirmProps) { 
  const {name, iconName, isDisable, onClick, color = "primary", key = "00", ...dataProps} = props;
  const renderContent = (): JSX.Element => {
    if(iconName && listIcon[iconName]) {
      return <IconButton onClick={() => onClick()} color={color} disabled={isDisable} sx={{border: '1px solid #ddd', mr: 1, mb: 1}}>
        {listIcon[iconName]}
      </IconButton>
    }else{
      return <Button variant="text" sx={{ height: "100%", border: '1px solid #ddd', mr: 1, mb: 1}} onClick={() => onClick()} disabled={isDisable} color={color}>
        {name}
      </Button>
    }
  }

  return (
    <>
      <Tooltip title={name} key={key} disableInteractive>
        <span style={isDisable ? { pointerEvents: "none" } : {}}>
          {renderContent()}
        </span>
      </Tooltip>
    </>
  )
}

export default withTranslation()(ActionBtn);
