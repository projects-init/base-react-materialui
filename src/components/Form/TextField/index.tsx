import { useField, FieldHookConfig } from "formik";
import { TextField, TextFieldProps } from "@mui/material";

const TextFieldFmik = (props: TextFieldProps & FieldHookConfig<string>) => {
  const [field, meta] = useField(props);
  return (
    <>
      <TextField
        {...field}
        {...props}
        id={props.name}
        label={props.label}
        name={props.name}
        fullWidth
        error={meta.touched && Boolean(meta.error)}
        helperText={meta.touched && meta.error}
      />
    </>
  );
};

export default TextFieldFmik;
