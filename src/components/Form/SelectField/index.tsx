import { useField, FieldHookConfig } from "formik";
import { MenuItem, TextField, TextFieldProps } from "@mui/material";

const SelectFieldFmik = (props: TextFieldProps & FieldHookConfig<string> & any) => {
  const [field, meta] = useField(props);
  return (
    <>
      <TextField
        {...field}
        {...props}
        autoFocus
        select
        id={props.name}
        label={props.label}
        name={props.name}
        fullWidth
        error={meta.touched && Boolean(meta.error)}
        helperText={meta.touched && meta.error}
      >
        {props?.options.map((option: any) => (
            <MenuItem key={option?.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
      </TextField>
    </>
  );
};

export default SelectFieldFmik;
