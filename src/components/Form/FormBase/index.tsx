
import { Formik, FormikProps, Form } from "formik";

const FormFmk = ({ children, initValue, validationSchema, onSubmit }) => {
  return (
    <Formik initialValues={initValue} onSubmit={onSubmit} validationSchema={validationSchema}>
      {(props: FormikProps<{}>) => <Form>{children}</Form>}
    </Formik>
  );
};
export default FormFmk;
