/**
 * @param {number} input
 * @return number formatted, 100000 => 100,000
 */
export const formatNumber = (number: number) => {
    const value = number + '';
    const list = value.split('.');
    const prefix = list[0].charAt(0) === '-' ? '-' : '';
    let num = prefix ? list[0].slice(1) : list[0];
    let result = '';
    while (num.length > 3) {
        result = `,${num.slice(-3)}${result}`;
        num = num.slice(0, num.length - 3);
    }
    if (num) {
        result = num + result;
    }
    return `${prefix}${result}${list[1] ? `.${list[1]}` : ''}`;
};

/**
 * @param func function debounced
 * @param waitFor time debounce
 */
export const debounce = (func, waitFor) => {
    let timeout = 0;

    const debounced = (...args) => {
        clearTimeout(timeout);
        setTimeout(() => func(...args), waitFor);
    };

    return debounced;
};

/**
 * @param {number} input
 * @return number formatted, 1 => 01
 */
export const getStandardIntegerNumberFormatByValue = (value: any) => {
    if (isNaN(value) || value == 0) {
        return '0'
    }

    return parseInt(value) >= 10 ? parseInt(value).toString() : `0${parseInt(value)}`
}