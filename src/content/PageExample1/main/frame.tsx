import { Container } from "@mui/material";
import PageTitle from "src/components/PageTitle";
import PageTitleWrapper from "src/components/PageTitleWrapper";
import Content from "./content";

const Frame = ({}: any) => {
    return (
        <>
            {/* Title page */}
            <PageTitleWrapper>
                <PageTitle
                    heading="title_page_py"
                    subHeading="Something"
                />
            </PageTitleWrapper>
            
            {/* Content page */}
            <Container maxWidth="xl">
                <Content />
            </Container>

            {/* Other Components used in page such as: diaglog, popper or something else */}
        </>
    )
};

export default Frame;