import loadable from '../../../components/Loadable';

export const ContainerPage = loadable(
  () => import('./container')
);