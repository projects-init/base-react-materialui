import { Grid } from "@mui/material";
import React from "react";
import { FC, forwardRef } from "react";

const Content: FC<any> = forwardRef(({}, ref): JSX.Element => {
    return (
        <>
            <Grid
                container
                direction="row"
                justifyContent="center"
                alignItems="stretch"
                spacing={3}
            >
              Content here
            </Grid>
        </>
    )
});

export default Content;