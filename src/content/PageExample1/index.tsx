import { memo } from 'react';
import { ContainerPage } from './main/loadable';

const PageExample = memo(() => {
    return (
        <>
            <ContainerPage />
        </>
    )
})

export default PageExample