import Loader from 'src/components/Loader';


const Category = Loader(() => import("src/content/Modules/PageExample"));

export const pageExamplePath = [
  {
    path: 'category',
    element: <Category />,
    active: true,
  },
];
