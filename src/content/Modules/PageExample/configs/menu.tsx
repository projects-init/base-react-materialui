import { MenuItems } from "src/layouts/SidebarLayout/Sidebar/SidebarMenu/items";
import BackupTableIcon from '@mui/icons-material/BackupTable';
import BrightnessLowTwoToneIcon from '@mui/icons-material/BrightnessLowTwoTone';
import PersonIcon from '@mui/icons-material/Person';

const categoryMenu: MenuItems[] = [
    {
      id: '0',
      heading: '',
      role: ["admin"],
      items: [
        {
          id: '0_1',
          parent: '0',
          name: 'category',
          link: '/category',
          icon: PersonIcon,
          role: ["admin"],
        }
      ],
    }
  ];
  
  export default categoryMenu;
  