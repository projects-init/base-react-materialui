import React from "react";
import { Button } from "@mui/material";
import { FC, forwardRef } from "react";
import PageLayout from "src/layouts/PageLayout";
import Table from "../../../components/Table";
import AddIcon from "@mui/icons-material/Add";
import { GridColDef } from "@mui/x-data-grid";

const PageExample: FC<any> = forwardRef(({}, ref): JSX.Element => {

  const columns: GridColDef[] = [];

  return (
    <>
      <PageLayout title={"Page Example"}>
        <Button
          variant="outlined"
          startIcon={<AddIcon />}
          sx={{ marginBottom: 1 }}
          onClick={() => console.log(1)}
        >
          Thêm
        </Button>
        <Table data={[]} columns={columns} />
      </PageLayout>

      {/* Other Components used in page such as: diaglog, popper or something else */}
    </>
  );
});

export default PageExample;
