import { createSlice } from '@reduxjs/toolkit';
import { BREAK_POINT_SCREEN, LOCAL_STORAGE_KEY } from 'src/constants/common';

const initialState = {
    isOpenSidebar: true,
    currentIotHub: localStorage.getItem(LOCAL_STORAGE_KEY.CURRENT_HUB) ?  localStorage.getItem(LOCAL_STORAGE_KEY.CURRENT_HUB) : null,
    alert: {
        openStatus: false,
        type: '',
        title: '',
        body: '',
        actions: null,
        url: ''
    },
    currentScreenSize:{ winWidth: window.innerWidth, winHeight: window.innerHeight}
}

export default createSlice({
    name: 'app',
    initialState,
    reducers: {
        toggleCollapseSideBar: (state) => ({
            ...state,
            isOpenSidebar: !state.isOpenSidebar
        }),
        switchIoTHubSuccess: (state, action) => ({
            ...state,
            currentIotHub: action.payload.result
        }),
        setAlertStausSuc: (state, action) => ({
            ...state,
            alert: {
                openStatus: true,
                type: action.payload.type,
                title: action.payload.title,
                body: action.payload.body,
                actions: action.payload.actions,
                url: action.payload.url
            }
        }),
        resetAlertDialog: (state) => ({
            ...state,
            alert: {
                openStatus: false,
                type: "",
                title: "",
                body: "",
                actions: null,
                url: ""
            }
        }),
        setScreenSize: (state, action) => ({
            ...state,
            currentScreenSize: action.payload,
            isOpenSidebar: action.payload.winWidth < BREAK_POINT_SCREEN && !state.isOpenSidebar ?  true : state.isOpenSidebar 
        })
    }
});