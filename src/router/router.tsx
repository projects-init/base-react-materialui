
import SidebarLayout from 'src/layouts/SidebarLayout';
import BaseLayout from 'src/layouts/BaseLayout';

import { Navigate } from 'react-router';
import { pageExamplePath } from 'src/content/Modules/PageExample/configs/path';
import Loader from 'src/components/Loader';

//Auth
const Login = Loader(() => import('src/content/Modules/Login'));
// const LoginAuth = Loader(lazy(() => import('src/content/pages/Login'))); // use for SSO Login

// Status
const Status404 = Loader(() => import('src/content/Modules/Status/Status404'));
const Status500 = Loader(() => import('src/content/Modules/Status/Status500'));
const StatusComingSoon = Loader(() => import('src/content/Modules/Status/ComingSoon'));
const StatusMaintenance = Loader(() => import('src/content/Modules/Status/Maintenance'));

const routes =
  [
    {
      path: '/',
      element: <BaseLayout />,
      children: [
        {
          path: '404',
          element: <Status404 />
        },
        {
          path: '500',
          element: <Status500 />
        },
        {
          path: 'maintenance',
          element: <StatusMaintenance />
        },
        {
          path: 'coming-soon',
          element: <StatusComingSoon />
        },
        {
          path: '*',
          element: <Navigate to={"/category"} />
        },
        {
          path: '',
          element: <Navigate to={"/category"} />
        },
      ]
    },
    //rnf
    {
      path: 'login',
      element: (
        <BaseLayout />
      ),
      children: [
        {
          path: '',
          element: <Login />
        },
      ]
    },
    //rnf
    {
      path: '',
      element: (
        <SidebarLayout />
      ),
      children: [
        ...pageExamplePath,
      ]
    },
  ];

export default routes;
