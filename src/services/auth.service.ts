
import axios from "axios";
import { CONFIG_APP, LOCAL_STORAGE_KEY } from "src/constants/common";

const API_URL = CONFIG_APP.API_URL;

const register = (username: string, email: string, password: string) => {
  return axios.post(`${API_URL}login`, {
    username,
    email,
    password,
  });
};

const login = (body: any): any => {
  return axios
    .post(`${API_URL}auth/local`, body)
    .then((response: any) => {
      if (response.data?.jwt) {
        localStorage.setItem(
          LOCAL_STORAGE_KEY.ACCESS_TOKEN_APP,
          response.data?.jwt
        );
      }
      return response.data;
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};

const logout = () => {
  localStorage.removeItem(LOCAL_STORAGE_KEY.ACCESS_TOKEN_APP);
};

const authService = {
  register,
  login,
  logout,
};

export default authService;